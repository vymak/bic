﻿using BIC02.Functions;
using BIC02.Objects;
using ILNumerics;
using ILNumerics.Drawing;
using ILNumerics.Drawing.Plotting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIC02
{
    public partial class Main : Form
    {
        private static ILScene scena;
        private bool _threadPaused = false;
        private Thread animace;
        private ILArray<float> data;
        private int firstPopulationSize = 50;
        private Population graph;

        private bool intOnly = false;
        private Minimum minForm;
        private Thread pocitat;
        private Population population2;
        private int populationSize = 10000;
        private ILSurface surface;

        private EventWaitHandle wh = new AutoResetEvent(false);

        public Main()
        {
            InitializeComponent();
            graphpopulationUpDown1.Value = populationSize;
            firstPopulationUpDown2.Minimum = 5;
            firstPopulationUpDown2.Value = firstPopulationSize;
            graphpopulationUpDown1.ValueChanged += NumericUpDown1_ValueChanged;
            firstPopulationUpDown2.ValueChanged += NumericUpDown1_ValueChanged;
            pocitat = new Thread(ZacitPocitat);
            GenerateLabels();
        }

        public void ShowPointThread(object obj)
        {
            List<Population> list = (List<Population>)obj;

            int i = 0;
            foreach (Population item in list)
            {
                surface = new ILSurface(data);
                surface.Fill.Markable = false;
                surface.Wireframe.Markable = false;
                this.ShowPointsOnGraph(item, 8);
                this.RefreshGraph();
                i++;
                Invoke(new Action(() => nameFunctionLabel.Text = "Iterace: " + i.ToString() + ", Minumum: " + item.population.Min(x => x.fitnessValue)));
                Thread.Sleep(1000);

                if (_threadPaused)
                    wh.WaitOne();
            }
            Invoke(new Action(() => playPausePictureBox.Visible = false));
        }

        private void BlindButton_Click(object sender, EventArgs e)
        {
            BlindAlghoritm blind = new BlindAlghoritm(surface, (int)firstPopulationUpDown2.Value);
            this.functionButtonSet(false);
            this.RefreshGraph();

            minForm = new Minimum(blind.minima, FunctionName.type.ToString());
            minForm.Show();
        }

        private void DifferentialButton_Click(object sender, EventArgs e)
        {
            List<Population> tmp = new DiferentialAlghoritm().ComputeDifferentialEvolution(population2);
            this.functionButtonSet(false);

            animace = new Thread(ShowPointThread);
            animace.IsBackground = true;
            animace.Start(tmp);
        }

        private void functionButtonSet(bool value)
        {
            Invoke(new Action(() =>
            {
                _threadPaused = value;
                playPausePictureBox.Visible = !value;
                differentialButton.Enabled = value;
                blindButton.Enabled = value;
                somaButton.Enabled = value;
                somaAllToAll.Enabled = value;
            }
                ));
        }

        private void GenerateLabels()
        {
            int height = 40;
            foreach (var item in Enum.GetValues(typeof(FunctionName.Type)))
            {
                LinkLabel l = new LinkLabel();
                l.Text = item.ToString();
                l.Location = new System.Drawing.Point(20, height);
                l.Name = item.ToString();
                l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238))); ;
                l.LinkColor = System.Drawing.Color.Gray;
                l.ActiveLinkColor = System.Drawing.Color.DarkGray;

                l.Click += LabelClick;
                panel2.Controls.Add(l);

                height += 28;
            }
        }

        private void CheckBoxSetting_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == firstPopulationChechBox)
            {
                if (firstPopulationChechBox.Checked)
                    intOnly = true;
                else
                    intOnly = false;
                ManageMenu();
            }
        }

        private void LabelClick(object sender, EventArgs e)
        {
            if (minForm != null)
                minForm.Close();

            foreach (Control item in panel2.Controls)
            {
                if (item.GetType() == typeof(LinkLabel))
                {
                    if (item == (LinkLabel)sender)
                    {
                        FunctionName.type = (FunctionName.Type)Enum.Parse(typeof(FunctionName.Type), item.Name.ToString());
                        nameFunctionLabel.Text = item.Text;
                    }
                }
            }
            ManageMenu();
        }

        private void ManageMenu()
        {
            this.functionButtonSet(true);

            if (pocitat.IsAlive)
                pocitat.Abort();

            pocitat = new Thread(ZacitPocitat);
            pocitat.IsBackground = true;
            pocitat.Start();
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (sender == graphpopulationUpDown1)
            {
                populationSize = (int)graphpopulationUpDown1.Value;
                ManageMenu();
            }
            else if (sender == firstPopulationUpDown2)
                firstPopulationSize = (int)firstPopulationUpDown2.Value;
        }

        private void playPauseClick(object sender, EventArgs e)
        {
            if (animace != null)
            {
                if (animace.IsAlive)
                {
                    if (_threadPaused)
                    {
                        _threadPaused = false;
                        playPausePictureBox.Image = Properties.Resources._1385568086_pause;
                        wh.Set();
                    }
                    else
                    {
                        _threadPaused = true;
                        playPausePictureBox.Image = Properties.Resources._1385568066_play;
                    }
                }
            }
        }

        private void RefreshGraph()
        {
            Invoke(new Action(() =>
            {
                scena = new ILScene { new ILPlotCube(twoDMode: false) { surface } };
                //scena.First<ILPlotCube>().Rotation = Matrix4.Rotation(new Vector3(1f, 0.25f, 1f), 0.7f);
                ilPanel1.Scene = scena;
                ilPanel1.Refresh();
            }));
        }

        private void ShowPointsOnGraph(Population tmp, int size = 10)
        {
            double best = tmp.population.Min(x => x.fitnessValue);
            foreach (Individual item in tmp.population)
            {
                ILArray<float> coords = new float[3];
                coords[0] = (float)item.parameters[0];
                coords[1] = (float)item.parameters[1];
                coords[2] = (float)item.fitnessValue + 1000;
                ILPoints bod = surface.Add(Shapes.Point);

                if (item.fitnessValue == best)
                {
                    bod.Color = Color.Red;
                    bod.Size = 20;
                }
                else
                {
                    bod.Color = Color.Black;
                    bod.Size = size;
                }

                bod.Positions.Update(coords);
                surface.Add(bod);
            }
        }

        private void somaAllToAll_Click(object sender, EventArgs e)
        {
            List<Population> tmp = new SomaAllToAll(population2).ComputeSoma();
            this.functionButtonSet(false);

            animace = new Thread(ShowPointThread);
            animace.IsBackground = true;
            animace.Start(tmp);
        }

        private void somaButton_Click(object sender, EventArgs e)
        {
            List<Population> tmp = new SomaAllToOne(population2).ComputeSoma();
            this.functionButtonSet(false);

            animace = new Thread(ShowPointThread);
            animace.IsBackground = true;
            animace.Start(tmp);
        }

        private void ZacitPocitat()
        {
            Invoke(new Action(() => ilPanel1.Visible = false));
            Invoke(new Action(() => nameFunctionLabel.Visible = false));

            graph = new Population(populationSize, FunctionName.type);
            graph.SetBoundsAndGeneratePopulation();
            graph.GeneratePopulation();
            graph.CalculateFitness();

            population2 = new Population(firstPopulationSize, FunctionName.type);
            population2.SetBoundsAndGeneratePopulation();
            population2.GenerateFirstPopulation(intOnly);
            population2.CalculateFitness();

            float[] newData = new float[graph.population.Count * 3];
            Parallel.For(0, graph.population.Count, i =>
            {
                newData[i] = (float)graph.population[i].fitnessValue;  // Z
                newData[i + graph.population.Count] = (float)graph.population[i].parameters[0]; // X
                newData[i + 2 * graph.population.Count] = (float)graph.population[i].parameters[1]; // Y
            });

            int size = (int)Math.Sqrt(graph.population.Count);
            data = ILNumerics.ILMath.array(newData, size, size, 3);

            surface = new ILSurface(data);
            surface.Fill.Markable = false;
            surface.Wireframe.Markable = false;

            /*this.ShowPointsOnGraph(population2);*/

            this.RefreshGraph();
            Invoke(new Action(() => nameFunctionLabel.Visible = true));
            Invoke(new Action(() => ilPanel1.Visible = true));
        }
    }
}