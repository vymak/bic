﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    public static class EggHolder
    {
        public const double minBound = -512;
        public const double maxBound = 512;

        public static void setFitness(Individual ind)
        {
            double x1 = ind.parameters[0];
            double x2 = ind.parameters[1];
            ind.fitnessValue = -(x2 + 47) * Math.Sin(Math.Sqrt(Math.Abs(x2 + (x1 / 2.0) + 47))) - x1 * Math.Sin(Math.Sqrt(Math.Abs(x1 - (x2 + 47)))); 
        }
    }
}
