﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    /// <summary>
    /// Class for SineEnvelope alghoritm
    /// </summary>
    /// <see cref="http://www.maths.uq.edu.au/CEToolBox/node3.html#SECTION000211000000000000000"/>
    public static class SineEnvelope
    {
        public const double minBound = -12;
        public const double maxBound = 12;

        public static void setFitness(Individual ind)
        {
            double x1 = ind.parameters[0];
            double x2 = ind.parameters[1];

            ind.fitnessValue = (Math.Pow(Math.Sin(Math.Sqrt(Math.Pow(x2, 2) + Math.Pow(x1, 2))), 2)) / Math.Pow(0.001 * (Math.Pow(x2, 2) + Math.Pow(x1, 2)) + 1, 2) + 0.5;
        }
    }
}
