﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIC02.Functions
{
    public static class FunctionName
    {
        /// <summary>
        /// Promměnná uchovávající funkci, která se počítá
        /// </summary>
        public static Type type { get; set; }

        /// <summary>
        /// Výběr funkcí pro generování
        /// </summary>
        public enum Type { Ackley1, Ackley2, DeJong1, DeJong3, DeJong4, EggHolder, Griewank, Michalewicz, Paretova, Rastrigin, Rosenbrock, Schwefel, SineEnvelope };
    }
}
