﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    /// <summary>
    /// Class for Schwefel alghoritm
    /// </summary>
    /// <see cref="http://www.sfu.ca/~ssurjano/schwef.html"/>
    public static class Schwefel
    {
        public const double minBound = -500;
        public const double maxBound = 500;

        public static void setFitness(Individual ind)
        {
            double temp = 0.0;
            foreach (double parametr in ind.parameters)
                temp -= (parametr * Math.Sin(Math.Sqrt(Math.Abs(parametr))));
            ind.fitnessValue = (temp);
        }
    }
}
