﻿using BIC02.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIC02.Functions
{
    /// <summary>
    /// Class for Michalewicz alghoritm
    /// </summary>
    /// <see cref="http://www.sfu.ca/~ssurjano/michal.html"/>
    public class Michalewicz
    {
        public const double minBound = 0;
        public const double maxBound = Math.PI;
        private const int m = 10;

        public static void setFitness(Individual ind)
        {
            double temp = 0.0;

            int i = 1;
            foreach (double parametr in ind.parameters)
            {
                temp += Math.Sin(parametr) * Math.Pow(Math.Sin((i * Math.Pow(parametr, 2))/Math.PI), 2 * m);
                i++;
            }
            ind.fitnessValue = - temp;
        }
    }
}
