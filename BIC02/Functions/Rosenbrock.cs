﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    public static class Rosenbrock
    {
        public const double minBound = -2.048;
        public const double maxBound = 2.048;
        //WTF index out of array

        public static void setFitness(Individual ind)
        {
            ind.fitnessValue = Math.Pow((1 - ind.parameters[0]), 2) + 100 * Math.Pow((ind.parameters[1] - Math.Pow(ind.parameters[0], 2)), 2);
        }
    }
}
