﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    public static class Griewank
    {
        public const double minBound = -600;
        public const double maxBound = 600;

        public static void setFitness(Individual ind)
        {
            double x1 = ind.parameters[0];
            double x2 = ind.parameters[1];
            double temp = 0.0;
            foreach (double parameter in ind.parameters)
            {
                temp += Math.Pow(parameter, 2.0) / 4000.0;
            }
            ind.fitnessValue = temp - getProduct(ind); 
        }

        private static double getProduct(Individual ind)
        {
            double result = 1.0;
            int i = 1;
            foreach (double parameter in ind.parameters)
            {
                result *= Math.Cos(parameter / Math.Sqrt(i)) + 1;
                i++;
            }
            return result;
        }
    }
}
