﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    /// <summary>
    /// Class for Ackley alghoritm
    /// </summary>
    /// <see cref="http://www.sfu.ca/~ssurjano/ackley.html"/>
    public static class Ackley2
    {
        public const double minBound = -20.768;
        public const double maxBound = 20.768;
        private const double a = 20.0;
        private const double b = 0.2;
        private const double c = 2.0 * Math.PI;

        public static void setFitness(Individual ind)
        {
            ind.fitnessValue = -a * Math.Pow(Math.E, (-b * Math.Sqrt(0.5 * getSum1(ind)))) - Math.Pow(Math.E, (0.5 * getSum2(ind))) + a + Math.E;
        }

        private static double getSum1(Individual ind)
        {
            double temp = 0.0;
            foreach (double parametr in ind.parameters)
            {
                temp += Math.Pow(parametr, 2);
            }

            return temp;
        }

        private static double getSum2(Individual ind)
        {
            double temp = 0.0;
            foreach (double parametr in ind.parameters)
            {
                temp += Math.Cos(c * parametr);
            }

            return temp;
        }
    }
}