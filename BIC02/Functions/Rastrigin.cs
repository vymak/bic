﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    public static class Rastrigin
    {
        public const double minBound = -5.12;
        public const double maxBound = 5.12;

        public static void setFitness(Individual ind)
        {
            foreach (double parametr in ind.parameters)
                ind.fitnessValue += (Math.Pow(parametr, 2) - 10 * Math.Cos(2 * Math.PI * parametr));
            //Konstanta * počet parametrů
            ind.fitnessValue += 10 * 2;
        }
    }
}
