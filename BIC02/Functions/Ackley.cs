﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BIC02.Objects;

namespace BIC02.Functions
{
    /// <summary>
    /// Class for Ackley alghoritm
    /// </summary>
    /// <see cref="http://www.sfu.ca/~ssurjano/ackley.html"/>
    public static class Ackley
    {
        public const double minBound = -20;
        public const double maxBound = 20;

        public static void setFitness(Individual ind)
        {
            double x1 = ind.parameters[0];
            double x2 = ind.parameters[1];
            ind.fitnessValue = (1/(Math.Pow(Math.E,5))) * (Math.Sqrt((Math.Pow(x1,2)+Math.Pow(x2,2))) + 3*(Math.Cos(2*x1) + Math.Sin(2*x2)));
        }
    }
}