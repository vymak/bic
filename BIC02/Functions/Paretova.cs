﻿using BIC02.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIC02.Functions
{
    public class Paretova
    {
        public static double minBound = 0;
        public static double maxBound = 1;

        public static void setFitness(Individual ind)
        {
            double x1 = ind.parameters[0];
            double x2 = ind.parameters[1] + 10;
            double alpha = 0.25 + 3.75 * ((x2 - 12) / (11 - 12));
            ind.fitnessValue = Math.Pow((x1 / x2), alpha) - (x1 / x2) * Math.Sin(Math.PI * x1 * x2);
        }
    }
}
