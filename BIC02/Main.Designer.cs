﻿namespace BIC02
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.playPausePictureBox = new System.Windows.Forms.PictureBox();
            this.nameFunctionLabel = new System.Windows.Forms.Label();
            this.ilPanel1 = new ILNumerics.Drawing.ILPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.somaAllToAll = new System.Windows.Forms.Button();
            this.somaButton = new System.Windows.Forms.Button();
            this.differentialButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.firstPopulationUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.firstPopulationChechBox = new System.Windows.Forms.CheckBox();
            this.blindButton = new System.Windows.Forms.Button();
            this.graphpopulationUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playPausePictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstPopulationUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphpopulationUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.playPausePictureBox);
            this.panel1.Controls.Add(this.nameFunctionLabel);
            this.panel1.Controls.Add(this.ilPanel1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(834, 568);
            this.panel1.TabIndex = 10;
            // 
            // playPausePictureBox
            // 
            this.playPausePictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.playPausePictureBox.Image = global::BIC02.Properties.Resources._1385568086_pause;
            this.playPausePictureBox.Location = new System.Drawing.Point(786, 520);
            this.playPausePictureBox.Name = "playPausePictureBox";
            this.playPausePictureBox.Size = new System.Drawing.Size(48, 48);
            this.playPausePictureBox.TabIndex = 3;
            this.playPausePictureBox.TabStop = false;
            this.playPausePictureBox.Visible = false;
            this.playPausePictureBox.Click += new System.EventHandler(this.playPauseClick);
            // 
            // nameFunctionLabel
            // 
            this.nameFunctionLabel.AutoSize = true;
            this.nameFunctionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nameFunctionLabel.ForeColor = System.Drawing.Color.DimGray;
            this.nameFunctionLabel.Location = new System.Drawing.Point(-3, 0);
            this.nameFunctionLabel.Name = "nameFunctionLabel";
            this.nameFunctionLabel.Size = new System.Drawing.Size(18, 26);
            this.nameFunctionLabel.TabIndex = 2;
            this.nameFunctionLabel.Text = " ";
            // 
            // ilPanel1
            // 
            this.ilPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ilPanel1.Driver = ILNumerics.Drawing.RendererTypes.GDI;
            this.ilPanel1.Editor = null;
            this.ilPanel1.Location = new System.Drawing.Point(0, 0);
            this.ilPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ilPanel1.Name = "ilPanel1";
            this.ilPanel1.Rectangle = ((System.Drawing.RectangleF)(resources.GetObject("ilPanel1.Rectangle")));
            this.ilPanel1.ShowUIControls = false;
            this.ilPanel1.Size = new System.Drawing.Size(834, 568);
            this.ilPanel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.somaAllToAll);
            this.panel2.Controls.Add(this.somaButton);
            this.panel2.Controls.Add(this.differentialButton);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.firstPopulationUpDown2);
            this.panel2.Controls.Add(this.firstPopulationChechBox);
            this.panel2.Controls.Add(this.blindButton);
            this.panel2.Controls.Add(this.graphpopulationUpDown1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(852, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 568);
            this.panel2.TabIndex = 12;
            // 
            // somaAllToAll
            // 
            this.somaAllToAll.Enabled = false;
            this.somaAllToAll.Location = new System.Drawing.Point(19, 431);
            this.somaAllToAll.Name = "somaAllToAll";
            this.somaAllToAll.Size = new System.Drawing.Size(96, 23);
            this.somaAllToAll.TabIndex = 10;
            this.somaAllToAll.Text = "Soma AllToAll";
            this.somaAllToAll.UseVisualStyleBackColor = true;
            this.somaAllToAll.Click += new System.EventHandler(this.somaAllToAll_Click);
            // 
            // somaButton
            // 
            this.somaButton.Enabled = false;
            this.somaButton.Location = new System.Drawing.Point(121, 431);
            this.somaButton.Name = "somaButton";
            this.somaButton.Size = new System.Drawing.Size(96, 23);
            this.somaButton.TabIndex = 9;
            this.somaButton.Text = "Soma OneToAll";
            this.somaButton.UseVisualStyleBackColor = true;
            this.somaButton.Click += new System.EventHandler(this.somaButton_Click);
            // 
            // differentialButton
            // 
            this.differentialButton.Enabled = false;
            this.differentialButton.Location = new System.Drawing.Point(121, 460);
            this.differentialButton.Name = "differentialButton";
            this.differentialButton.Size = new System.Drawing.Size(96, 23);
            this.differentialButton.TabIndex = 8;
            this.differentialButton.Text = "Diferenciální";
            this.differentialButton.UseVisualStyleBackColor = true;
            this.differentialButton.Click += new System.EventHandler(this.DifferentialButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(89, 540);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "velikost prvop. populace";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(88, 514);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "populace pro graf";
            // 
            // firstPopulationUpDown2
            // 
            this.firstPopulationUpDown2.ForeColor = System.Drawing.Color.Gray;
            this.firstPopulationUpDown2.Location = new System.Drawing.Point(19, 538);
            this.firstPopulationUpDown2.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.firstPopulationUpDown2.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.firstPopulationUpDown2.Name = "firstPopulationUpDown2";
            this.firstPopulationUpDown2.Size = new System.Drawing.Size(63, 20);
            this.firstPopulationUpDown2.TabIndex = 5;
            this.firstPopulationUpDown2.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // firstPopulationChechBox
            // 
            this.firstPopulationChechBox.AutoSize = true;
            this.firstPopulationChechBox.ForeColor = System.Drawing.Color.Gray;
            this.firstPopulationChechBox.Location = new System.Drawing.Point(19, 489);
            this.firstPopulationChechBox.Name = "firstPopulationChechBox";
            this.firstPopulationChechBox.Size = new System.Drawing.Size(104, 17);
            this.firstPopulationChechBox.TabIndex = 4;
            this.firstPopulationChechBox.Text = "pouze celá čísla";
            this.firstPopulationChechBox.UseVisualStyleBackColor = true;
            this.firstPopulationChechBox.CheckedChanged += new System.EventHandler(this.CheckBoxSetting_CheckedChanged);
            // 
            // blindButton
            // 
            this.blindButton.Enabled = false;
            this.blindButton.Location = new System.Drawing.Point(19, 460);
            this.blindButton.Name = "blindButton";
            this.blindButton.Size = new System.Drawing.Size(96, 23);
            this.blindButton.TabIndex = 3;
            this.blindButton.Text = "Slepý algoritmus";
            this.blindButton.UseVisualStyleBackColor = true;
            this.blindButton.Click += new System.EventHandler(this.BlindButton_Click);
            // 
            // graphpopulationUpDown1
            // 
            this.graphpopulationUpDown1.ForeColor = System.Drawing.Color.Gray;
            this.graphpopulationUpDown1.Location = new System.Drawing.Point(19, 512);
            this.graphpopulationUpDown1.Maximum = new decimal(new int[] {
            400000,
            0,
            0,
            0});
            this.graphpopulationUpDown1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.graphpopulationUpDown1.Name = "graphpopulationUpDown1";
            this.graphpopulationUpDown1.Size = new System.Drawing.Size(63, 20);
            this.graphpopulationUpDown1.TabIndex = 2;
            this.graphpopulationUpDown1.Value = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(14, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seznam funkcí";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1104, 592);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Text = "Aplikace pro výpočet a zobrazení některých funkcí";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playPausePictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.firstPopulationUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphpopulationUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private ILNumerics.Drawing.ILPanel ilPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown graphpopulationUpDown1;
        private System.Windows.Forms.Label nameFunctionLabel;
        private System.Windows.Forms.Button blindButton;
        private System.Windows.Forms.CheckBox firstPopulationChechBox;
        private System.Windows.Forms.NumericUpDown firstPopulationUpDown2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button differentialButton;
        private System.Windows.Forms.Button somaButton;
        private System.Windows.Forms.Button somaAllToAll;
        private System.Windows.Forms.PictureBox playPausePictureBox;
    }
}

