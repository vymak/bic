﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace BIC02
{
    public partial class Minimum : Form
    {
        public static Color[] barvy =
        {
            Color.Blue,
            Color.Red,
            Color.Yellow,
            Color.Magenta,
            Color.Lime,
            Color.Maroon,
            Color.HotPink,
            Color.BurlyWood,
            Color.Cornsilk,
            Color.DarkOrchid,
            Color.DarkTurquoise,
            Color.DeepSkyBlue,
            Color.Gainsboro
        };

        private Dictionary<int, double> minima;

        public Minimum(Dictionary<int, double> minima, string function)
        {
            InitializeComponent();
            this.Text = function.ToUpper() + " minima";
            this.minima = minima;
            GenerateDataToGridView();
        }

        private void GenerateDataToGridView()
        {
            int counter = 0;
            foreach (KeyValuePair<int, double> item in minima)
            {
                dataGridView1.Rows.Add(null, item.Key, String.Format("{0:0.0000000000000}", item.Value));
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataGridViewCellStyle style = new DataGridViewCellStyle();
                style.BackColor = barvy[counter];
                row.Cells[0].Style = style;

                counter++;

                if (counter == barvy.Length)
                    counter = 0;
            }
        }
    }
}