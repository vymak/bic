﻿using BIC02.Functions;
using System;

namespace BIC02.Objects
{
    [Serializable]
    public class Individual
    {
        public double[] parameters = new double[2];

        public Individual()
        {
        }

        public Individual(double param1, double param2)
        {
            this.parameters[0] = param1;
            this.parameters[1] = param2;
        }

        public double fitnessValue { get; set; }

        public static void calculateFitnessForIndividuum(Individual ind)
        {
            switch (FunctionName.type)
            {
                case FunctionName.Type.Schwefel:
                    Schwefel.setFitness(ind);
                    break;

                case FunctionName.Type.DeJong1:
                    DeJong1.setFitness(ind);
                    break;

                case FunctionName.Type.DeJong3:
                    DeJong3.setFitness(ind);
                    break;

                case FunctionName.Type.DeJong4:
                    DeJong4.setFitness(ind);
                    break;

                case FunctionName.Type.Rastrigin:
                    Rastrigin.setFitness(ind);
                    break;

                case FunctionName.Type.Rosenbrock:
                    Rosenbrock.setFitness(ind);
                    break;

                case FunctionName.Type.EggHolder:
                    EggHolder.setFitness(ind);
                    break;

                case FunctionName.Type.Griewank:
                    Griewank.setFitness(ind);
                    break;

                case FunctionName.Type.Michalewicz:
                    Michalewicz.setFitness(ind);
                    break;

                case FunctionName.Type.Ackley1:
                    Ackley.setFitness(ind);
                    break;

                case FunctionName.Type.Ackley2:
                    Ackley2.setFitness(ind);
                    break;

                case FunctionName.Type.SineEnvelope:
                    SineEnvelope.setFitness(ind);
                    break;

                case FunctionName.Type.Paretova:
                    Paretova.setFitness(ind);
                    break;
            }
        }
    }
}