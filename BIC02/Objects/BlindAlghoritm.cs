﻿using BIC02.Functions;
using ILNumerics;
using ILNumerics.Drawing;
using ILNumerics.Drawing.Plotting;
using System;
using System.Collections.Generic;

namespace BIC02.Objects
{
    /// <summary>
    /// Třída pro nalezení minima pomocí slepého algoritmu
    /// </summary>
    public class BlindAlghoritm
    {
        /// <summary>
        /// Konstruktor pro vytvoření nově instance
        /// </summary>
        /// <param name="surface">Aktuální surface</param>
        /// <param name="count">Velikost populace</param>
        public BlindAlghoritm(ILSurface surface, int count = 100)
        {
            this.populationSize = count;
            this.minima = new Dictionary<int, double>();
            this.surface = surface;
            this.FindMinByBlindAlghoritm();
        }

        /// <summary>
        /// Dictionary uchovávající nalezená minima a iteraci
        /// </summary>
        public Dictionary<int, double> minima { get; private set; }

        /// <summary>
        /// Proměnná uchovávající velikost populace pro generování
        /// </summary>
        private int populationSize { get; set; }

        /// <summary>
        /// Proměnná uchovávající surface
        /// </summary>
        private ILSurface surface { get; set; }

        /// <summary>
        /// Hledání minima pomocí blind algoritmu
        /// </summary>
        private void FindMinByBlindAlghoritm()
        {
            int iteraci = 100;
            int cBarva = 0;
            double globalMin = Double.MaxValue;
            for (int i = 0; i < iteraci; i++)
            {
                Population pop = new Population(this.populationSize, FunctionName.type);
                pop.SetBoundsAndGeneratePopulation();
                pop.GenerateFirstPopulation();
                pop.CalculateFitness();

                double min = Double.MaxValue;
                foreach (var item in pop.population)
                {
                    if (item.fitnessValue < min)
                        min = item.fitnessValue;
                }

                if (min < globalMin)
                {
                    globalMin = min;
                    this.minima.Add(i, min);

                    foreach (Individual ind in pop.population)
                    {
                        ILArray<float> coords = new float[3];
                        coords[0] = (float)ind.parameters[0];
                        coords[1] = (float)ind.parameters[1];
                        coords[2] = (float)ind.fitnessValue + 1000;

                        if (ind.fitnessValue == min)
                        {
                            ILPoints bod = surface.Add(Shapes.Point);
                            bod.Size = 10;
                            bod.Color = Minimum.barvy[cBarva];
                            bod.Positions.Update(coords);
                            surface.Add(bod);
                        }
                    }
                    cBarva++;
                }
            }
        }
    }
}