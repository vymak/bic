﻿using BIC02.Functions;
using System;
using System.Collections.Generic;

namespace BIC02.Objects
{
    public class Population
    {
        public List<Individual> population = new List<Individual>();

        public Population(FunctionName.Type type)
        {
            this.type = type;
        }

        public Population(int populationSize, FunctionName.Type type)
        {
            this.populationSize = populationSize;
            this.type = type;
        }

        public Population(int populationSize)
        {
            this.populationSize = populationSize;
        }

        public double maxBound { get; private set; }

        public double minBound { get; private set; }

        public int populationSize { get; private set; }

        public FunctionName.Type type { get; private set; }

        private int axisTicks { get; set; }

        private double entireInterval { get; set; }

        private double interval { get; set; }

        public void GenerateFirstPopulation(bool integer = false)
        {
            Individual ind;
            Random r = new Random();

            for (int i = 0; i < populationSize; i++)
            {
                double x1 = 0;
                double x2 = 0;

                if (!integer)
                {
                    x1 = r.NextDouble() * (maxBound - minBound) + minBound;
                    x2 = r.NextDouble() * (maxBound - minBound) + minBound;
                }
                else
                {
                    x1 = r.Next((int)minBound, (int)maxBound + 1);
                    x2 = r.Next((int)minBound, (int)maxBound + 1);
                }

                ind = new Individual(x1, x2);
                this.population.Add(ind);
            }
        }

        public void GeneratePopulation()
        {
            Individual ind;
            double param1;
            double param2;
            entireInterval = this.maxBound - this.minBound;
            axisTicks = (int)Math.Sqrt(this.populationSize) / 2;
            interval = entireInterval / (axisTicks * 2);

            for (int i = -axisTicks; i <= axisTicks; i++)
                for (int j = -axisTicks; j <= axisTicks; j++)
                {
                    param1 = (i + axisTicks) * (entireInterval / (2 * axisTicks)) + minBound;
                    param2 = (j + axisTicks) * (entireInterval / (2 * axisTicks)) + minBound;
                    ind = new Individual(param1, param2);
                    this.population.Add(ind);
                }
        }

        /// <summary>
        /// Metoda počítající fitness funkce
        /// </summary>
        internal void CalculateFitness()
        {
            for (int j = 0; j < population.Count; j++)
            {
                switch (type)
                {
                    case FunctionName.Type.Schwefel:
                        Schwefel.setFitness(population[j]);
                        break;

                    case FunctionName.Type.DeJong1:
                        DeJong1.setFitness(population[j]);
                        break;

                    case FunctionName.Type.DeJong3:
                        DeJong3.setFitness(population[j]);
                        break;

                    case FunctionName.Type.DeJong4:
                        DeJong4.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Rastrigin:
                        Rastrigin.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Rosenbrock:
                        Rosenbrock.setFitness(population[j]);
                        break;

                    case FunctionName.Type.EggHolder:
                        EggHolder.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Griewank:
                        Griewank.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Michalewicz:
                        Michalewicz.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Ackley1:
                        Ackley.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Ackley2:
                        Ackley2.setFitness(population[j]);
                        break;

                    case FunctionName.Type.SineEnvelope:
                        SineEnvelope.setFitness(population[j]);
                        break;

                    case FunctionName.Type.Paretova:
                        Paretova.setFitness(population[j]);
                        break;
                }
            }
        }

        internal void SetBoundsAndGeneratePopulation()
        {
            switch (type)
            {
                case FunctionName.Type.Schwefel:
                    minBound = Schwefel.minBound;
                    maxBound = Schwefel.maxBound;
                    break;

                case FunctionName.Type.DeJong1:
                    minBound = DeJong1.minBound;
                    maxBound = DeJong1.maxBound;
                    break;

                case FunctionName.Type.DeJong3:
                    minBound = DeJong3.minBound;
                    maxBound = DeJong3.maxBound;
                    break;

                case FunctionName.Type.DeJong4:
                    minBound = DeJong4.minBound;
                    maxBound = DeJong4.maxBound;
                    break;

                case FunctionName.Type.Rastrigin:
                    minBound = Rastrigin.minBound;
                    maxBound = Rastrigin.maxBound;
                    break;

                case FunctionName.Type.Rosenbrock:
                    minBound = Rosenbrock.minBound;
                    maxBound = Rosenbrock.maxBound;
                    break;

                case FunctionName.Type.EggHolder:
                    minBound = EggHolder.minBound;
                    maxBound = EggHolder.maxBound;
                    break;

                case FunctionName.Type.Griewank:
                    minBound = Griewank.minBound;
                    maxBound = Griewank.maxBound;
                    break;

                case FunctionName.Type.Michalewicz:
                    minBound = Michalewicz.minBound;
                    maxBound = Michalewicz.maxBound;
                    break;

                case FunctionName.Type.Ackley1:
                    minBound = Ackley.minBound;
                    maxBound = Ackley.maxBound;
                    break;

                case FunctionName.Type.Ackley2:
                    minBound = Ackley2.minBound;
                    maxBound = Ackley2.maxBound;
                    break;

                case FunctionName.Type.SineEnvelope:
                    minBound = SineEnvelope.minBound;
                    maxBound = SineEnvelope.maxBound;
                    break;

                case FunctionName.Type.Paretova:
                    minBound = Paretova.minBound;
                    maxBound = Paretova.maxBound;
                    break;
            }
        }
    }
}