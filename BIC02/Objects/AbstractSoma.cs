﻿using System;
using System.Collections.Generic;

namespace BIC02.Objects
{
    public abstract class AbstractSoma
    {
        /// <summary>
        /// Určuje počet iterací
        /// </summary>
        protected const int _iteraci = 20;

        /// <summary>
        /// Délka cesty
        /// </summary>
        protected const double _pathLenght = 4.8;

        /// <summary>
        /// Perturbační vektor
        /// </summary>
        protected const double _prt = 0.3;

        /// <summary>
        /// Délka kroku
        /// </summary>
        protected const double _step = 0.13;

        /// <summary>
        /// Celkový počet kroků
        /// </summary>
        protected const int _stepCount = (int)(_pathLenght / _step);

        /// <summary>
        /// První populace
        /// </summary>
        protected Population pop;

        /// <summary>
        /// Random generátor pro perturbační vektor
        /// </summary>
        protected Random rand;
        protected AbstractSoma(Population pop)
        {
            this.pop = pop;
            _minBound = pop.minBound;
            _maxBound = pop.maxBound;
            _popSize = pop.population.Count;
            rand = new Random();
        }

        /// <summary>
        /// Maximální hranice
        /// </summary>
        protected double _maxBound { get; set; }

        /// <summary>
        /// Minimální hranice
        /// </summary>
        protected double _minBound { get; set; }

        /// <summary>
        /// Velikost populace
        /// </summary>
        protected int _popSize { get; set; }

        /// <summary>
        /// Abstraktní metoda pro výpočet SOMA algoritmu
        /// </summary>
        /// <returns>List<Population></returns>
        public abstract List<Population> ComputeSoma();

        /// <summary>
        /// Vypočítání skoku
        /// </summary>
        /// <param name="current">Aktuální jedinec</param>
        /// <param name="j"></param>
        /// <param name="vDirection">Individuum Direction vektor</param>
        /// <param name="prtVector">Individuum Perturbační vektor</param>
        /// <param name="k"></param>
        /// <returns>Individual</returns>
        protected Individual CalculateJump(Individual current, Individual vDirection, Individual prtVector, int k)
        {
            double x = current.parameters[0] + k * _step * vDirection.parameters[0] * prtVector.parameters[0];
            double y = current.parameters[1] + k * _step * vDirection.parameters[1] * prtVector.parameters[1];
            Individual tmp = new Individual(x, y);
            return tmp;
        }

        /// <summary>
        /// Vytvoří vektor směru
        /// </summary>
        /// <param name="current">Aktuální jedinec</param>
        /// <param name="best">Nejlepší jedinec z dané populace</param>
        /// <returns>Individual</returns>
        protected virtual Individual CreateDirectionVector(Individual current, Individual best)
        {
            Individual vDirection = new Individual();
            for (int k = 0; k < 2; k++)
                vDirection.parameters[k] = best.parameters[k] - current.parameters[k];
            return vDirection;
        }

        /// <summary>
        /// Vytvoření pertrubačního vektoru
        /// </summary>
        /// <returns>Individual</returns>
        protected virtual Individual CreatePrtVector()
        {
            Individual prtVector = new Individual();
            for (int k = 0; k < 2; k++)
            {
                if (rand.NextDouble() < _prt)
                    prtVector.parameters[k] = 1;
                else
                    prtVector.parameters[k] = 0;
            }
            return prtVector;
        }

        /// <summary>
        /// Nalezení nejlepšího jedince z dané populace
        /// </summary>
        /// <param name="prevouis"></param>
        /// <returns></returns>
        protected virtual Individual FindBestIndividual(Population prevouis)
        {
            Individual best = new Individual();
            best.fitnessValue = Double.MaxValue;

            // nalezení nejlepšího jedince v rámci populace
            for (int k = 0; k < _popSize; k++)
            {
                if (best.fitnessValue > prevouis.population[k].fitnessValue)
                    best = prevouis.population[k];
            }
            return best;
        }

        /// <summary>
        /// Otestování, zda nové individuum splňuje požadované meze
        /// </summary>
        /// <param name="tmp">Individual</param>
        /// <returns>bool</returns>
        protected virtual bool checkIsInBound(Individual tmp)
        {
            for (int l = 0; l < 2; l++)
            {
                if (tmp.parameters[l] < _minBound || tmp.parameters[l] > _maxBound)
                    return false;
            }
            return true;
        }
    }
}