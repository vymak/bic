﻿using System;
using System.Collections.Generic;

namespace BIC02.Objects
{
    public class SomaAllToAll : AbstractSoma
    {
        public SomaAllToAll(Population pop)
            : base(pop)
        {
        }

        public override List<Population> ComputeSoma()
        {
            List<Population> popList = new List<Population>();
            popList.Add(pop);

            for (int i = 1; i < _iteraci; i++)
            {
                Population prevouis = popList[i - 1];

                // další výpočty se somou
                Population popTmp = new Population(_popSize, pop.type);
                for (int j = 0; j < _popSize; j++)
                {
                    Individual bestJumper = new Individual();
                    bestJumper.fitnessValue = Double.MaxValue;
                    Population popJumper = new Population(pop.type);
                    for (int k = 0; k < _popSize; k++)
                    {
                        if (j == k)
                        {
                            popJumper.population.Add(prevouis.population[k]);
                            continue;
                        }

                        Individual vDirection = CreateDirectionVector(prevouis.population[j], prevouis.population[k]);
                        Individual prtVector = CreatePrtVector();

                        for (int l = 0; l < _stepCount; l++)
                        {
                            Individual tmp = CalculateJump(prevouis.population[j], vDirection, prtVector, l);

                            if (checkIsInBound(tmp))
                            {
                                Individual.calculateFitnessForIndividuum(tmp);
                                if (tmp.fitnessValue < bestJumper.fitnessValue)
                                    bestJumper = tmp;
                            }
                        }
                        popJumper.population.Add(bestJumper);
                    }
                    popTmp.population.Add(FindBestIndividual(popJumper));
                }
                popList.Add(popTmp);
            }
            return popList;
        }
    }
}