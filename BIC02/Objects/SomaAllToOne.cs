﻿using System.Collections.Generic;

namespace BIC02.Objects
{
    public class SomaAllToOne : AbstractSoma
    {
        public SomaAllToOne(Population pop)
            : base(pop)
        {
        }

        public override List<Population> ComputeSoma()
        {
            List<Population> popList = new List<Population>();
            popList.Add(pop);

            for (int i = 1; i < _iteraci; i++)
            {
                Population popTmp = new Population(_popSize, pop.type);
                Population prevouis = popList[i - 1];
                Individual best = FindBestIndividual(prevouis);

                // další výpočty se somou
                for (int j = 0; j < _popSize; j++)
                {
                    Individual vDirection = CreateDirectionVector(prevouis.population[j], best);
                    Individual prtVector = CreatePrtVector();

                    Individual bestJumper = new Individual();
                    bestJumper.fitnessValue = double.MaxValue;
                    for (int k = 0; k < _stepCount; k++)
                    {
                        Individual tmp = CalculateJump(prevouis.population[j], vDirection, prtVector, k);
                        if (checkIsInBound(tmp))
                        {
                            Individual.calculateFitnessForIndividuum(tmp);
                            if (bestJumper.fitnessValue > tmp.fitnessValue)
                                bestJumper = tmp;
                        }
                    }
                    popTmp.population.Add(bestJumper);
                }
                popList.Add(popTmp);
            }
            return popList;
        }
    }
}