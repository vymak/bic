﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BIC02.Objects
{
    public class DiferentialAlghoritm
    {
        /// <summary>
        /// CR konstanta
        /// </summary>
        private const double _CR = 0.8;

        /// <summary>
        /// Dimenze dat
        /// </summary>
        private const int _dimension = 2;

        /// <summary>
        /// Šumivá konstanta
        /// </summary>
        private const double _F = 0.4;

        /// <summary>
        /// Konstanta udávající počet iterací
        /// </summary>
        private const int _iteraci = 99;

        public List<Population> ComputeDifferentialEvolution(Population p)
        {
            Random r = new Random();
            double minBound = p.minBound;
            double maxBound = p.maxBound;

            List<Population> generations = new List<Population>();
            generations.Add(p);

            for (int i = 1; i <= _iteraci; i++)
            {
                Population currentGen = new Population(p.type);
                int currentID = 0;
                foreach (Individual current in generations.Last().population)
                {
                    List<int> numbers = new List<int>();
                    numbers.Add(currentID);
                    while (numbers.Count != 4)
                    {
                        int next = r.Next(0, generations.Last().population.Count);
                        if (!numbers.Contains(next))
                            numbers.Add(next);
                    }
                    Individual first = ((Individual)generations.Last().population[numbers[0]]);
                    Individual second = ((Individual)generations.Last().population[numbers[1]]);
                    Individual third = ((Individual)generations.Last().population[numbers[2]]);

                    Individual noise = new Individual();
                    noise.parameters[0] = (((first.parameters[0] - second.parameters[0]) * _F) + third.parameters[0]);
                    noise.parameters[1] = (((first.parameters[1] - second.parameters[1]) * _F) + third.parameters[1]);

                    Individual best = new Individual();

                    bool isCrossed = false;
                    while (isCrossed != true)
                    {
                        for (int j = 0; j < 2; j++)
                        {
                            double value = r.NextDouble();
                            if (value > _CR)
                            {
                                best.parameters[j] = noise.parameters[j];
                                isCrossed = true;
                            }
                            else
                                best.parameters[j] = current.parameters[j];
                        }
                    }

                    for (int j = 0; j < 2; j++)
                    {
                        if (best.parameters[j] > maxBound || best.parameters[j] < minBound)
                        {
                            best = CreateIndividual(minBound, maxBound);
                            break;
                        }
                    }
                    currentGen.population.Add(best);
                    currentID++;
                }
                currentGen.CalculateFitness();

                for (int j = 0; j < currentGen.population.Count; j++)
                {
                    if (currentGen.population[j].fitnessValue > generations[i - 1].population[j].fitnessValue)
                        currentGen.population[j] = generations[i - 1].population[j];
                }
                generations.Add(currentGen);
            }
            return generations;
        }

        private static Individual CreateIndividual(double minBound, double maxBound)
        {
            Random ind = new Random();
            double x1 = ind.NextDouble() * (maxBound - minBound) + minBound;
            double x2 = ind.NextDouble() * (maxBound - minBound) + minBound;
            return new Individual(x1, x2);
        }
    }
}